package com.codingforcookies.betterrecords.src;

import com.google.common.collect.Maps;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.world.World;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustStrategy;

import javax.net.ssl.HttpsURLConnection;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Map;

public class BetterUtils {

    private static Map<String, ChatComponentTranslation> translations = Maps.newHashMap();
    private static String lang = "en_US";

    public static void markBlockDirty(World par1World, int x, int y, int z) {
        if (par1World.blockExists(x, y, z)) par1World.getChunkFromBlockCoords(x, z).setChunkModified();
    }

    public static String getTranslatedString(String str) {
        ChatComponentTranslation cct = translations.get(str);
        if (cct != null) {
            if (!Minecraft.getMinecraft().gameSettings.language.equals(lang)) {
                lang = Minecraft.getMinecraft().gameSettings.language;
                translations.clear();
                return new ChatComponentTranslation(str).getFormattedText();
            }
            return cct.getFormattedText();
        } else {
            cct = new ChatComponentTranslation(str);
            translations.put(str, cct);
            return cct.getFormattedText();
        }
    }

    public static HttpURLConnection makeHttpsConnection(URL url) throws Exception {
        URLConnection url_conn = url.openConnection();
        if (!(url_conn instanceof HttpURLConnection))
            throw new MalformedURLException("Connection is not HTTP");

        if (url_conn instanceof HttpsURLConnection) {
            HttpsURLConnection https = (HttpsURLConnection) url_conn;
            SSLContextBuilder builder = new SSLContextBuilder();
            builder.loadTrustMaterial(null, new TrustStrategy() {
                public boolean isTrusted(final X509Certificate[] chain, String authType) throws CertificateException {
                    return true;
                }
            });
            https.setSSLSocketFactory(builder.build().getSocketFactory());
        }

        return (HttpURLConnection) url_conn;
    }
}