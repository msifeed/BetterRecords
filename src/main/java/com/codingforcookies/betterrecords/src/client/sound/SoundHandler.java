package com.codingforcookies.betterrecords.src.client.sound;

import com.codingforcookies.betterrecords.src.BetterRecords;
import com.codingforcookies.betterrecords.src.BetterUtils;
import com.codingforcookies.betterrecords.src.ClasspathInjector;
import com.codingforcookies.betterrecords.src.betterenums.IRecordAmplitude;
import com.codingforcookies.betterrecords.src.betterenums.IRecordWireHome;
import com.codingforcookies.betterrecords.src.betterenums.RecordConnection;
import com.codingforcookies.betterrecords.src.client.ClientProxy;
import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.ModClassLoader;
import net.minecraft.client.Minecraft;
import net.minecraft.tileentity.TileEntity;

import javax.sound.sampled.*;
import javax.sound.sampled.DataLine.Info;
import java.io.*;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

public class SoundHandler {

    public static File soundLocation;
    public static HashMap<String, File> soundList;
    public static HashMap<String, SoundManager> soundPlaying;
    public static boolean downloadSongs = true;
    public static int streamBuffer = 256;
    public static boolean streamRadio = true;
    public static String nowPlaying = "";
    public static long nowPlayingEnd = 0;
    public static int nowPlayingInt = 0;

    private static File libs = new File(Minecraft.getMinecraft().mcDataDir, "betterrecords/libs");

    public static void initalize() {
        libs.mkdirs();

        loadLibrary("jl-1.0.1.jar");
        loadLibrary("jogg-0.0.7.jar");
        loadLibrary("jorbis-0.0.15.jar");
        loadLibrary("tritonus-share-0.3.7.jar");
        loadLibrary("mp3spi-1.9.5.jar");
        loadLibrary("vorbisspi-1.0.3.jar");
        loadLibrary("mp3plugin.jar");

        soundLocation = new File(Minecraft.getMinecraft().mcDataDir, "betterrecords/cache");
        soundLocation.mkdirs();
        soundList = new HashMap<>();
        soundPlaying = new HashMap<>();

        File[] cache_files = soundLocation.listFiles();
        if (cache_files != null) {
            for (File sound : cache_files) {
                System.out.println("Loaded " + sound.getName());
                soundList.put(sound.getName(), sound);
            }
        }
        System.out.println("Loaded sound cache of " + soundList.size() + " sounds.");
    }

    private static void loadLibrary(String lib_name) {
        File file = new File(libs, lib_name);
        if (!file.exists()) {
            final String libs_url = "https://gitlab.com/msifeed/BetterRecords/raw/1.7.10/lib/";
            FileDownloader.downloadFile(lib_name, libs_url + lib_name, lib_name, libs);
        }

        System.out.println("Injecting library " + file.getName() + ".");
        try {
            ((ModClassLoader) Loader.instance().getModClassLoader()).addFile(file);
        } catch (IOException e) {
            e.printStackTrace();
            BetterRecords.logger.error("Failed to load library, trying another method: " + file.getName());
            try {
                ClasspathInjector.addFile(file);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    public static void playSound(final int x, final int y, final int z, final int dimension, final float playRadius, final List<Sound> sounds, boolean repeat, boolean shuffle) {
        playSound(x, y, z, dimension, playRadius, sounds, repeat, shuffle, 0);
    }

    private static void obtainSound(final SoundManager manager, final int songIndex) {
        soundList.put(manager.songs.get(songIndex).name, new File(soundLocation, manager.songs.get(songIndex).name));
        new Thread() {

            public void run() {
                if (!FileDownloader.downloadFile(soundLocation, manager, songIndex))
                    soundList.remove(manager.songs.get(songIndex).name);
                else playSound(manager, songIndex);
            }
        }.start();
    }

    protected static void playSound(SoundManager manager, int songIndex) {
        if (songIndex == -1)
            playSound(manager.songs.get(0).x, manager.songs.get(0).y, manager.songs.get(0).z, manager.songs.get(0).dimension, manager.songs.get(0).playRadius, manager.songs, manager.repeat, manager.shuffle, -1);
        else
            playSound(manager.songs.get(songIndex).x, manager.songs.get(songIndex).y, manager.songs.get(songIndex).z, manager.songs.get(songIndex).dimension, manager.songs.get(songIndex).playRadius, manager.songs, manager.repeat, manager.shuffle, songIndex);
    }

    private static void playSound(final int x, final int y, final int z, final int dimension, final float playRadius, final List<Sound> sounds, boolean repeat, boolean shuffle, int songIndex) {
        if (songIndex >= 0) {
            SoundManager sndMgr = null;
            if (soundPlaying.get(x + "," + y + "," + z + "," + dimension) == null) {
                sndMgr = new SoundManager(repeat, shuffle);
                sndMgr.songs = sounds;
                soundPlaying.put(x + "," + y + "," + z + "," + dimension, sndMgr);
            } else sndMgr = soundPlaying.get(x + "," + y + "," + z + "," + dimension);
            for (int i = songIndex; i < sounds.size(); i++) {
                if (!soundList.containsKey(sounds.get(i).name)) {
                    if (downloadSongs) {
                        if (FileDownloader.isDownloading) {
                            BetterRecords.logger.error("Song downloading... Please wait...");
                            nowPlaying = BetterUtils.getTranslatedString("overlay.nowplaying.error1");
                            nowPlayingEnd = System.currentTimeMillis() + 5000;
                            return;
                        }
                        obtainSound(sndMgr, i);
                    }
                    return;
                } else {
                    if (!soundList.get(sounds.get(i).name).exists()) {
                        BetterRecords.logger.error("Sound exists in hashmap, but not in file. Reloading cache...");
                        soundList.clear();
                        if (!soundLocation.mkdirs()) {
                            for (File soundFile : soundLocation.listFiles()) {
                                System.out.println("Loaded " + soundFile.getName());
                                soundList.put(soundFile.getName(), soundFile);
                            }
                        }
                        i--;
                    } else if (i == 0) {
                        tryToStart(x, y, z, dimension);
                        if (sounds.size() == 1) return;
                    }
                }
            }
        }
        tryToStart(x, y, z, dimension);
    }

    private static void tryToStart(final int x, final int y, final int z, final int dimension) {
        final String sound_id = x + "," + y + "," + z + "," + dimension;
        final SoundManager manager = soundPlaying.get(sound_id);

        if (manager != null && manager.current == -1)
            new Thread(new Runnable() {

                public void run() {
                    Sound snd = null;
                    if (manager.current != -1) return;
                    while (soundPlaying.get(sound_id) != null && (snd = manager.nextSong()) != null) {
                        nowPlaying = snd.local;
                        nowPlayingEnd = System.currentTimeMillis() + 5000;

                        File audio_file = new File(soundLocation, snd.name);
                        try {
                            AudioInputStream audio = AudioSystem.getAudioInputStream(audio_file.toURI().toURL());
                            playSourceDataLine(snd, x, y, z, dimension, BetterSoundType.SONG, audio);
                            audio.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                            nowPlaying = BetterUtils.getTranslatedString("overlay.nowplaying.error3");
                            BetterRecords.logger.error("Failed to play record: " + snd.name);
                        }
                    }
                    soundPlaying.remove(sound_id);
                }
            }).start();
    }

    public static void playSoundFromStream(final int x, final int y, final int z, final int dimension, final float playRadius, final String localName, final String url) {
        if (!streamRadio) return;
        soundPlaying.put(x + "," + y + "," + z + "," + dimension, new SoundManager(new Sound(x, y, z, dimension, playRadius).setInfo("", url, localName), false, false));
        new Thread(new Runnable() {

            public void run() {
                try {
                    System.out.println("Connection to stream " + localName + "...");
                    Sound snd = soundPlaying.get(x + "," + y + "," + z + "," + dimension).nextSong();
                    IcyURLConnection urlConn = new IcyURLConnection(new URL(url.startsWith("http") ? url : "http://" + url));
                    urlConn.setInstanceFollowRedirects(true);
                    urlConn.connect();

                    BufferedInputStream bis = new BufferedInputStream(urlConn.getInputStream());
                    AudioInputStream audio = AudioSystem.getAudioInputStream(bis);
                    playSourceDataLine(snd, x, y, z, dimension, BetterSoundType.RADIO, audio);
                } catch (Exception e) {
                    e.printStackTrace();
                    BetterRecords.logger.error("Failed to stream: " + url);
                    nowPlaying = BetterUtils.getTranslatedString("overlay.nowplaying.error2");
                    nowPlayingEnd = System.currentTimeMillis() + 5000;
                }
                soundPlaying.remove(x + "," + y + "," + z + "," + dimension);
            }
        }).start();
    }

    private static void playSourceDataLine(Sound snd, int x, int y, int z, int dimension, BetterSoundType type, AudioInputStream audio) {
        try {
            convertToPCM(audio);

            AudioFormat outFormat = getOutFormat(audio.getFormat());
            Info info = new Info(SourceDataLine.class, outFormat);
            System.out.println("Playing " + snd.name + ": " + new File(soundLocation, snd.name).getAbsolutePath());

            try (SourceDataLine line = (SourceDataLine) AudioSystem.getLine(info)) {
                if (line != null) {
                    line.open(outFormat);
                    if (snd.volume == null) {
                        snd.volume = (FloatControl) line.getControl(FloatControl.Type.MASTER_GAIN);
                        snd.volume.setValue(-20F);
                    }
                    line.start();

                    stream(AudioSystem.getAudioInputStream(outFormat, audio), line, x, y, z, dimension, type);
                    line.drain();
                    line.stop();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            switch (type) {
                case SONG:
                    nowPlaying = BetterUtils.getTranslatedString("overlay.nowplaying.error3");
                    BetterRecords.logger.error("Could not read file: Local: " + snd.local + " File: " + snd.name);
                    break;
                case RADIO:
                    nowPlaying = BetterUtils.getTranslatedString("overlay.nowplaying.error2");
                    BetterRecords.logger.error("Failed to stream: URL: " + snd.url);
                    break;
                default:
                    break;
            }
            nowPlayingEnd = System.currentTimeMillis() + 5000;
        }
    }

    private static AudioFormat getOutFormat(AudioFormat inFormat) {
        final int ch = inFormat.getChannels();
        final float rate = inFormat.getSampleRate();
        return new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, rate, 16, ch, ch * 2, rate, false);
    }

    private static void stream(AudioInputStream in, SourceDataLine line, int x, int y, int z, int dimension, BetterSoundType soundType) throws IOException {
        final byte[] buffer = new byte[streamBuffer];
        for (int n = 0; n != -1; n = in.read(buffer, 0, buffer.length)) {
            while (Minecraft.getMinecraft().isSingleplayer() && Minecraft.getMinecraft().currentScreen != null && Minecraft.getMinecraft().currentScreen.doesGuiPauseGame()) {
                try {
                    Thread.sleep(5L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (soundPlaying.get(x + "," + y + "," + z + "," + dimension) == null || (soundType == BetterSoundType.RADIO && !streamRadio))
                return;
            updateAmplitude(buffer, x, y, z, dimension);
            line.write(buffer, 0, n);
        }
        updateAmplitude(null, x, y, z, dimension);
    }

    private static void updateAmplitude(byte[] buffer, int x, int y, int z, int dimension) {
        if (Minecraft.getMinecraft().theWorld.provider.dimensionId != dimension) return;
        float unscaledTreble = -1F;
        float unscaledBass = -1F;
        TileEntity tileEntity = Minecraft.getMinecraft().theWorld.getTileEntity(x, y, z);
        if (tileEntity != null && tileEntity instanceof IRecordWireHome) {
            ((IRecordWireHome) tileEntity).addTreble(getUnscaledWaveform(buffer, true, false));
            ((IRecordWireHome) tileEntity).addBass(getUnscaledWaveform(buffer, false, false));
            for (RecordConnection con : ((IRecordWireHome) tileEntity).getConnections()) {
                if (buffer == null) return;
                TileEntity tileEntityCon = Minecraft.getMinecraft().theWorld.getTileEntity(con.x2, con.y2, con.z2);
                if (tileEntityCon != null && tileEntityCon instanceof IRecordAmplitude) {
                    if (unscaledTreble == -1F || unscaledBass == 11F) {
                        unscaledTreble = getUnscaledWaveform(buffer, true, true);
                        unscaledBass = getUnscaledWaveform(buffer, false, true);
                    }
                    ((IRecordAmplitude) tileEntityCon).setTreble(unscaledTreble);
                    ((IRecordAmplitude) tileEntityCon).setBass(unscaledBass);
                }
            }
        }
    }

    public static float getUnscaledWaveform(byte[] eightBitByteArray, boolean high, boolean control) {
        if (eightBitByteArray != null) {
            int[] toReturn = new int[eightBitByteArray.length / 2];
            float avg = 0;
            int index = 0;
            for (int audioByte = high ? 1 : 0; audioByte < eightBitByteArray.length; audioByte += 2) {
                toReturn[index] = (int) eightBitByteArray[audioByte];
                avg += toReturn[index];
                index++;
            }
            avg = avg / toReturn.length;
            if (control) {
                if (avg < 0F) avg = Math.abs(avg);
                if (avg > 20F) return (ClientProxy.flashyMode < 3 ? 10F : 20F);
                else return (int) (avg * (ClientProxy.flashyMode < 3 ? 1F : 2F));
            }
            return avg;
        }
        return 0;
    }

    private static int getSixteenBitSample(int high, int low) {
        return (high << 8) + (low & 0x00ff);
    }

    private static void convertToPCM(AudioInputStream audioInputStream) {
        AudioFormat format = audioInputStream.getFormat();
        AudioFormat.Encoding encoding = format.getEncoding();

        if ((encoding != AudioFormat.Encoding.PCM_SIGNED) && (encoding != AudioFormat.Encoding.PCM_UNSIGNED)) {
            AudioFormat targetFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, format.getSampleRate(),
                    16, format.getChannels(), format.getChannels() * 2,
                    format.getSampleRate(), format.isBigEndian());
            audioInputStream = AudioSystem.getAudioInputStream(targetFormat, audioInputStream);
        }
    }

    private enum BetterSoundType {
        SONG, RADIO;
    }
}