package com.codingforcookies.betterrecords.src.client.sound;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import com.codingforcookies.betterrecords.src.BetterRecords;
import com.codingforcookies.betterrecords.src.BetterUtils;
import com.codingforcookies.betterrecords.src.client.ClientProxy;
import com.codingforcookies.betterrecords.src.gui.GuiRecordEtcher;
import cpw.mods.fml.common.registry.LanguageRegistry;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

public class FileDownloader {
	public static String nowDownloading = "";
	public static float downloadPercent = 0F;
	public static boolean isDownloading = false;
	
	public static boolean downloadFile(File saveLoc, final SoundManager manager, final int songIndex) {
		if(isDownloading)
			return false;
		Sound sound = manager.songs.get(songIndex);
		return downloadFile(sound.name, sound.url, sound.local, saveLoc, new DownloadInfo() {
			private boolean started = false;
			public void onDownloading(String local, float percent) {
				if(songIndex == 0 && ClientProxy.playWhileDownload && !started && downloadPercent > 0.05F) {
					started = true;
					SoundHandler.playSound(manager, -1);
				}
			}
			
			public void onFileTooLarge(String local) {
				SoundHandler.nowPlaying = "Error: " + local + " is over download limit!";
				SoundHandler.nowPlayingEnd = System.currentTimeMillis() + 5000;
			}
			
			public void onGeneralFailure(String local) {
				SoundHandler.nowPlaying = "Error: General download error for " + local;
				SoundHandler.nowPlayingEnd = System.currentTimeMillis() + 5000;
			}
		});
	}
	
	public static boolean downloadFile(String name, String urlLocation, String localName, File saveLoc) {
		return downloadFile(name, urlLocation, localName, saveLoc, null);
	}
	
	private static boolean downloadFile(String name, String urlLocation, String localName, File saveLoc, DownloadInfo dwnloadNfo) {
		BetterRecords.logger.info("Downloading {} fom {}", name, urlLocation);
		isDownloading = true;
		nowDownloading = localName;
		downloadPercent = 0F;

		BufferedInputStream in = null;
		FileOutputStream out = null;

		try {
			URL url = new URL(urlLocation.replace(" ", "%20"));
			URLConnection url_conn = url.openConnection();
			if (!(url_conn instanceof HttpURLConnection))
				throw new MalformedURLException("Connection is not HTTP");

			HttpURLConnection conn = BetterUtils.makeHttpsConnection(url);
			conn.setRequestMethod("GET");
			conn.connect();
			
			int size = conn.getContentLength();
			if(size / 1024 / 1024 > (ClientProxy.downloadMax != 100 ? ClientProxy.downloadMax : 102400)) {
				BetterRecords.logger.error("File larger than configured max size!");
				if(dwnloadNfo != null)
					dwnloadNfo.onFileTooLarge(localName);
				return false;
			}
			
			in = new BufferedInputStream(url.openStream());
			out = new FileOutputStream(new File(saveLoc, name));
			byte data[] = new byte[1024];
			int count;
			float sumCount = 0F;
			
			while((count = in.read(data, 0, 1024)) != -1) {
				out.write(data, 0, count);
				sumCount += count;
				isDownloading = true;
				nowDownloading = localName;
				if(size > 0)
					downloadPercent = sumCount / size;
				
				if(dwnloadNfo != null)
					dwnloadNfo.onDownloading(localName, downloadPercent);
			}
			
			isDownloading = false;
			nowDownloading = "";
			return true;
		} catch (Exception e) {
			BetterRecords.logger.error("Failed to download", e);
			if(dwnloadNfo != null)
				dwnloadNfo.onGeneralFailure(localName);
		} finally {
			if(in != null)
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
					BetterRecords.logger.error("Failed to close inputstream while downloading: " + localName + "\247" + urlLocation);
				}
			if(out != null)
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
					BetterRecords.logger.error("Failed to close outputstream while downloading: " + localName + "\247" + urlLocation);
				}
		}
		
		isDownloading = false;
		nowDownloading = "";
		return false;
	}
	
	private static abstract class DownloadInfo {
		public abstract void onDownloading(String local, float percent);
		public abstract void onFileTooLarge(String local);
		public abstract void onGeneralFailure(String local);
	}
}